<?php

namespace Drupal\map_object_field\Plugin\views\style;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\map_object_field\MapObject\MapObjectList;
use Drupal\map_object_field\Service\MapObjectLibInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\map_object_field\MapObject\MapObjectDataMapper;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "single_map",
 *   title = @Translation("Single map"),
 *   help = @Translation("Shows entity field data as a map"),
 *   theme = "single_map_default",
 *   display_types = {"normal"}
 * )
 */
class SingleMap extends StylePluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = FALSE;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Map object field library service.
   *
   * @var \Drupal\map_object_field\Service\MapObjectLibInterface
   */
  protected $mapObjectFieldLib;

  /**
   * Contains methods to manipulate map objects.
   *
   * @var \Drupal\map_object_field\MapObject\MapObjectService
   */
  protected $mapObjectMapper;

  /**
   * Constructs StylePluginBase object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, MapObjectLibInterface $mapObjectFieldLib, MapObjectDataMapper $mapObjectMapper) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->mapObjectFieldLib = $mapObjectFieldLib;
    $this->mapObjectMapper = $mapObjectMapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('map_object_field_lib'),
      $container->get('map_object.mapper')
    );
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['type'] = ['default' => 'ul'];
    $options['wrapper_class'] = ['default' => 'map-wrapper'];
    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('List type'),
      '#options' => [
        'ul' => $this->t('Unordered list'),
        'ol' => $this->t('Ordered list'),
      ],
      '#default_value' => $this->options['type'],
    ];
    $form['wrapper_class'] = [
      '#title' => $this->t('Wrapper class'),
      '#description' => $this->t('The class to provide on the wrapper, outside map.'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['wrapper_class'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {

    if (empty($this->view->rowPlugin)) {
      trigger_error('Drupal\map_object_field\Plugin\views\style\SingleMap: Missing row plugin', E_WARNING);
      return [];
    }

    $entity_storage = [];
    $entities = [];
    // Attach js libraries.
    foreach ($this->mapObjectFieldLib->getLibrariesForWidget() as $lib) {
      $element['#attached']['library'][] = $lib;
    }

    // Get filtered enities.
    foreach ($this->view->result as $index => $row) {
      $typeId = $row->_entity->getEntityTypeId();
      if (empty($entity_storage[$typeId])) {
        $entity_storage[$typeId] = $this->entityTypeManager->getStorage($typeId);
      }
      $entity = $entity_storage[$typeId]->load($row->_entity->id());
      $entities[$typeId][] = [
        'nid' => $entity->id(),
        'revision' => $entity->getRevisionId(),
      ];
    }

    // Get map objects id by nid.
    $map_objects = $this->mapObjectMapper->getMapObjectsByArray($entities);

    // Leave only elements with last revison.
    array_walk($map_objects, function ($object, $key) use ($entities, &$map_objects) {
      foreach ($entities[$object->entity_type] as $entity) {
        if ($object->entity_revision_id == $entity['revision'] && $object->entity_id == $entity['nid']) {
          return;
        }
      }
      unset($map_objects[$key]);
    });

    // Create an array from $map_objects.
    $coordinates = $this->mapObjectMapper->getMapObjectCoordinates(array_keys($map_objects));
    $extra_params = $this->mapObjectMapper->getMapObjectExtraParams(array_keys($map_objects));
    $coordinates_per_object = [];
    foreach ($coordinates as $coordinate) {
      $coordinates_per_object[$coordinate->map_object_id][] = [
        'lat' => $coordinate->lat,
        'lng' => $coordinate->lng,
        'weight' => $coordinate->weight,
      ];
    }
    $extra_params_per_object = [];
    foreach ($extra_params as $extra_param) {
      $extra_params_per_object[$extra_param->map_object_id][$extra_param->extra_key] = $extra_param->extra_value;
    }
    $result = [];
    foreach ($map_objects as $map_object_id => $object) {
      $map_object_list = new MapObjectList($object);
      if (isset($coordinates_per_object[$map_object_id])) {
        $map_object_list->setObjectCoordinates($coordinates_per_object[$map_object_id]);
      }
      if (isset($extra_params_per_object[$map_object_id])) {
        $map_object_list->setExtraParams($extra_params_per_object[$map_object_id]);
      }
      $result[] = $map_object_list;
    }

    $overlays = json_encode(
      $result,
      JSON_HEX_TAG
      | JSON_HEX_APOS
      | JSON_HEX_AMP
      | JSON_HEX_QUOT
      | JSON_NUMERIC_CHECK
    );

    // Output custom fields as a list.
    foreach ($this->view->result as $row_index => $row) {
      $this->view->row_index = $row_index;
      $rows[] = $this->view->rowPlugin->render($row);
    }

    $build = [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#rows' => $rows,
      '#overlays' => $overlays,
    ];
    unset($this->view->row_index);
    return $build;
  }

}
