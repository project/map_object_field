<?php

namespace Drupal\map_object_field\MapObject;

use JsonSerializable;

/**
 * Map Object.
 */
class MapObjectList implements JsonSerializable {

  /**
   * The object type.
   *
   * @var string
   */
  protected $objectType = '';

  /**
   * The object coordinates.
   *
   * @var array
   */
  protected $objectCoordinates = [];

  /**
   * The extra params array.
   *
   * @var array
   */
  protected $extraParams = [];

  /**
   * The map definition array.
   *
   * @var array
   */
  protected $map = [
    'type' => 'objectType',
    'coordinates' => 'objectCoordinates',
    'extraParams' => 'extraParams',
  ];

  /**
   * Constructor.
   */
  public function __construct($data) {
    foreach ($data as $key => $val) {
      if (array_key_exists($key, $this->map)) {
        $set_method = 'set' . ucfirst($this->map[$key]);
        $this->$set_method($val);
      }
    }
  }

  /**
   * Getter for objectType.
   */
  public function getObjectType() {
    return $this->objectType;
  }

  /**
   * Setter for objectType.
   */
  public function setObjectType($object_type) {
    $this->objectType = $object_type;
  }

  /**
   * Getter for objectCoordinates.
   */
  public function getObjectCoordinates() {
    return $this->objectCoordinates;
  }

  /**
   * Setter for objectCoordinates.
   */
  public function setObjectCoordinates($object_coordiantes) {
    $this->objectCoordinates = $object_coordiantes;
  }

  /**
   * Getter for extraParams.
   */
  public function getExtraParams() {
    return $this->extraParams;
  }

  /**
   * Setter for extraParams.
   */
  public function setExtraParams($val) {
    $this->extraParams = $val;
  }

  /**
   * JsonSerializable intrface implementation.
   */
  public function jsonSerialize() {
    $inverted_map = array_flip($this->map);
    $result = [];
    foreach (get_object_vars($this) as $property_name => $property_value) {
      if (isset($inverted_map[$property_name])) {
        $result[$inverted_map[$property_name]] = $property_value;
      }
    }
    return $result;
  }

}
